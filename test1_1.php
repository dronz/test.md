<?php

	/**
	 * Validate any type of brackets
	 * @author kololobanov@gmail.com
	 * @see    https://stackoverrun.com/ru/q/85618
	 *
	 * @param string $str
	 * @param array  $brackets
	 *
	 * @return bool
	 */
	function validateBrackets(string $str, array $brackets = ['()', '[]', '{}', '<>']){
		foreach($brackets as $bracket){
			$depth = 0;
			for($i = 0; $i < strlen($str); $i++){
				$symbol = $str[$i];
				$depth  += $symbol == $bracket[0];
				$depth  -= $symbol == $bracket[1];
				//if any brace type start with closed brace - stop cycle
				if($depth < 0){
					return FALSE;
				}
			}
			if($depth !== 0){
				return FALSE;
			}
		}

		return TRUE;
	}

	/**
	 * same function, but with one string cycle (less readable). Also this is slower variant
	 *
	 * @param string $str
	 * @param array  $brackets
	 *
	 * @return bool
	 */
	function validateBrackets2(string $str, array $brackets = ['()', '[]', '{}', '<>']){
		$depth = array_fill_keys($brackets, 0);
		for($i = 0; $i < strlen($str); $i++){
			$symbol = $str[$i];
			foreach($brackets as $bracket){
				$depth[$bracket] += $symbol == $bracket[0];
				$depth[$bracket] -= $symbol == $bracket[1];
				if($depth[$bracket] < 0){
					return FALSE;
				}
			}
		}
		//find count on non-zero depths
		if(count(array_filter($depth, function ($v){ return $v !== 0; })) > 0){
			return FALSE;
		}

		return TRUE;
	}

	const TEST_DATA = [
		'[({})]'        => TRUE,
		'[({русский})]' => TRUE,
		'[({english})]' => TRUE,
		'[([)'          => FALSE,
		'[[][]]'        => TRUE,
		'((((()))))'    => TRUE,
		'<<<>>>'        => TRUE,
		'<<<)))'        => FALSE,
	];

	/**
	 * Unit test emulation
	 */
	function test(){
		echo "Start testing\n";
		foreach(TEST_DATA as $str => $result){
			if(validateBrackets2($str) === $result){
				echo "ok $str\n";
			}
			else{
				echo "error $str\n";
			}
		}
	}

	/**
	 * Perfomance testing
	 *
	 * @param callable $function
	 * @param int      $iteration
	 */
	function timeTest(callable $function, int $iteration = 10000){
		$start = microtime(TRUE);
		for($i = 0; $i < $iteration; $i++){
			foreach(TEST_DATA as $str => $result){
				call_user_func($function, $str);
			}
		}
		echo $function.' spend: '.(microtime(TRUE) - $start)."\n";
	}

	test();
	timeTest('validateBrackets');
	timeTest('validateBrackets2');