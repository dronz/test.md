-- https://stackoverflow.com/questions/688549/finding-duplicate-values-in-mysql

SELECT id, COUNT(*) c FROM test GROUP BY id HAVING c > 1;